# Opt Green: Nachhaltige Software Für Nachhaltige Hardware (NS4NH) ("Sustainable Software For Sustainable Hardware")

## Overview

"Opt Green" is a new KDE Eco project funded by the Umwelt Bundesamt. The goal of the project is to promote sustainable software and reducing e-waste by upcycling hardware with Free Software. The project will be funded from 1 April 2024 to 31 March 2026.

Through online and offline campaigns as well as installation workshops, KDE Eco will demonstrate at fair-trade, organic, and artisinal markets the power of Free Software to drive down energy consumption and keep devices in use for years beyond official vendor support. The offline campaigns will begin in Germany and around Europe and will later be taken to global communities with the support of KDE volunteers worldwide.

One target audience for the project are eco-consumers, those whose consumer behavior is driven by principles related to the environment, and not necesssarily convenience or cost.
