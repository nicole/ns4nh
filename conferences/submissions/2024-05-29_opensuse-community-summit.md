# "Opt Green", A New KDE Eco Initiative: Bringing Right-To-Repair Software To Consumers

Consumers want sustainable software. A 2020 poll found that 80% of European consumers want easier-to-repair digital devices, and 50% say two reasons they purchase new devices are performance issues and non-functioning software. Free Software can give consumers what they want, but many don't know it, yet! With the newly-funded project "Opt Green: Sustainable Software For Sustainable Hardware" KDE Eco will change that.

For two years, the "Opt Green" initiative will bring sustainable Free Software directly to consumers. This is a good time. In 2025, Windows 10 end-of-support is estimated to make e-waste of 240 million computers. In 2026 (at the earliest), macOS support for Intel-based Apple computers, the last sold in 2020, is expected to end, rendering millions of computers obsolete. These functioning, but vendor-abandoned devices can stay out of the landfill and in use for years with Free Software. (By comparison, Linux support for i486 processors lasted 23 years!)

By design, Free Software is right-to-repair software: users have control over their hardware by removing vendor dependencies and guaranteeing transparency and user autonomy. In this talk, I will present KDE Eco's new initiative. The target audience for the project are eco-consumers, those motivated by environmental concerns, not necessarily convenience. Through campaigns and workshops, we will demonstrate the power of Free Software at organic/artisanal markets to drive down energy consumption and keep devices in use for years beyond official vendor support. With software designed for users' needs, not vendors', it is possible to run efficient software on devices one already owns.

## Speaker Bio

Joseph P. De Veaugh-Geiss (he/him) is community manager for the KDE Eco initiative, which aims to strengthen ecological sustainability as part of the development and adoption of Free Software.
